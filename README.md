# Welcome to set09103 - Advanced Web Tech Course Work 1 #
### Author: John J Davidson - 40212121 ###
this repo is all source files for advanced web tech course work for deadline 26th of October 2016

# How to install
**1. First clone the repo via either HTTP or SSH**
### HTTP CLONE
> git clone https://Xuz99@bitbucket.org/Xuz99/cw1_set09103.git

###  SSH CLONE
> git clone git@bitbucket.org:Xuz99/cw1_set09103.git

**2. Make sure you have Python 2.7.12 installed and PIP as well as the web framework Flask**
#### Python
> https://www.python.org/downloads/

**To install pip on Ubuntu, Debian or Linux Mint:**

```$ sudo apt-get install python-pip```

**For other ways please follow the information in the link bellow**
> http://ask.xmodulo.com/install-pip-linux.html

#### Flask
> http://flask.pocoo.org/
___
# How to run

 - Once you have downloaded and cloned this repo enter a terminal and navigate using ```cd``` to the directory you have saved/cloned this repo and then run:

``` $ python app.py ```

 - Your terminal should now be running a local host server at port ```0.0.0.0:5000```. Use the browser to view this port by
 - entering this ```localhost``` or simply this ```localhost:5000```
