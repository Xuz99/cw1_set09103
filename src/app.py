#import code
import os, fnmatch
from flask import Flask, render_template, url_for, request, redirect
app = Flask(__name__)

@app.route('/')
def pepes():
	# Path to all images
	img_path = os.listdir(os.path.join(app.static_folder, 'img'))

	# Display happy pepes
	img_path_happy = os.listdir(os.path.join(app.static_folder, 'img/happy'))
	img = fnmatch.filter(img_path, '*.jpg')
	img_happy = fnmatch.filter(img_path_happy, '*.jpg')

	# Display sad pepes
	img_path_sad = os.listdir(os.path.join(app.static_folder, 'img/sad'))
	img = fnmatch.filter(img_path, '*.jpg')
	img_sad = fnmatch.filter(img_path_sad, '*.jpg')

	# Display angry pepes
	img_path_angry = os.listdir(os.path.join(app.static_folder, 'img/angry'))
	img = fnmatch.filter(img_path, '*.jpg')
	img_angry = fnmatch.filter(img_path_angry, '*.jpg')

	# Display smug pepes
	img_path_smug = os.listdir(os.path.join(app.static_folder, 'img/smug'))
	img = fnmatch.filter(img_path, '*.jpg')
	img_smug = fnmatch.filter(img_path_smug, '*.jpg')

	# render template for all imgs
	return render_template('pepes.html', img=img, img_happy=img_happy, img_sad=img_sad, img_angry=img_angry, img_smug=img_smug)

#end of pepe routes/filters

@app.route('/pepes/happy')
def happy_pepes():
	img_path = os.listdir(os.path.join(app.static_folder, 'img/happy'))
	img = fnmatch.filter(img_path, '*.jpg')
	return render_template('happy_pepes.html', img=img)

@app.route('/pepes/happy/upload_suc', methods = ['POST'])
def upload_happy():
	if request.method == 'POST':
		img_path = os.listdir(os.path.join(app.static_folder, 'img/happy'))
		img = fnmatch.filter(img_path, '*.jpg')

		img_file = request.files['file']
		img_name = request.files['file'].filename

		img_file.save('static/img/happy/' + img_name)
		return render_template('happy_pepes.html', img=img,)


@app.route('/pepes/sad')
def sad_pepes():
	img_path = os.listdir(os.path.join(app.static_folder, 'img/sad'))
	img = fnmatch.filter(img_path, '*.jpg')
	return render_template('sad_pepes.html', img=img)

@app.route('/pepes/sad/upload_suc', methods = ['POST'])
def upload_sad():
	if request.method == 'POST':
		img_path = os.listdir(os.path.join(app.static_folder, 'img/sad'))
		img = fnmatch.filter(img_path, '*.jpg')

		img_file = request.files['file']
		img_name = request.files['file'].filename

		img_file.save('static/img/sad/' + img_name)

		return render_template('sad_pepes.html', img=img)

@app.route('/pepes/angry')
def angry_pepes():
	img_path = os.listdir(os.path.join(app.static_folder, 'img/angry'))
	img = fnmatch.filter(img_path, '*.jpg')
	return render_template('angry_pepes.html', img=img)

@app.route('/pepes/angry/upload_suc', methods = ['POST'])
def upload_angry():
	if request.method == 'POST':
		img_path = os.listdir(os.path.join(app.static_folder, 'img/angry'))
		img = fnmatch.filter(img_path, '*.jpg')

		img_file = request.files['file']
		img_name = request.files['file'].filename

		img_file.save('static/img/angry/' + img_name)

		return render_template('angry_pepes.html', img=img)

@app.route('/pepes/smug')
def smug_pepes():
	img_path = os.listdir(os.path.join(app.static_folder, 'img/smug'))
	img = fnmatch.filter(img_path, '*.jpg')
	return render_template('smug_pepes.html', img=img)

@app.route('/pepes/smug/upload_suc', methods = ['POST'])
def upload_smug():
	if request.method == 'POST':
		img_path = os.listdir(os.path.join(app.static_folder, 'img/smug'))
		img = fnmatch.filter(img_path, '*.jpg')

		img_file = request.files['file']
		img_name = request.files['file'].filename

		img_file.save('static/img/smug/' + img_name)

		return render_template('smug_pepes.html', img=img)


if __name__ == '__main__':
	app.run(host='0.0.0.0', debug=True)
